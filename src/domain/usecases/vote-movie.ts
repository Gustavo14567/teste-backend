import { MovieModel } from '../models/movie'

export interface VoteMovieModel {
  title: string,
  grade: number,
  accountId: string
}

export interface VoteMovie {
  vote(voteMovie: VoteMovieModel): Promise<MovieModel>
}
